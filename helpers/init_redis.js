const redis = require('redis');

/**
 *  IF NO OPTIONS ARE PASSED TO createClient() IT WILL CONNECT TO CLIENT THAT IS HOSTED
 *  LOCALLY ON OUT MACHINE i.e PORT: 6379 and LOCALHOST: 127.0.0.1
 */
const client = redis.createClient({
  port: process.env.REDIS_PORT,
  host: process.env.REDIS_HOST,
});

/**
 *  TO SEE WHETHER THE CLIENT IS CONNECTED OR NOT WE HAVE CERTAIN EVENTS THAT
 *   ARE EMMITTED
 */

// CONNECT EVENT
client.on('connect', () => {
  console.log('Client connected to redis...');
});

// READY EVENT
client.on('ready', err => {
  console.log('Client connected to redis and ready to use...');
});

// ERROR EVENT
client.on('error', err => {
  console.log(err.message);
});

//END EVENT -  WHEN THE CLIENT IS DISCONNECTED FROM REDIS SERVER
client.on('end', () => {
  console.log('Client disconnected from redis');
});

// TO STOP REDIS WHENEVER WE PRESS CTRL-C WE USE SIGINT EVENT
process.on('SIGINT', () => {
  client.quit();
});

module.exports = client;
