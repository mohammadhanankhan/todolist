const joi = require('@hapi/joi');

const authSchema = joi.object({
  email: joi.string().email().lowercase().required(),
  password: joi.string().min(5).required(),
});

const taskSchema = joi.object({
  task: joi.string().required(),
  status: joi.string().required(),
});

const subtaskSchema = joi.object({
  subtask: joi.string().required(),
  status: joi.string().required(),
  task_id: joi.number().integer().required(),
});

module.exports = {
  authSchema,
  taskSchema,
  subtaskSchema,
};
