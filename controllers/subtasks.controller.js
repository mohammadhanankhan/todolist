const createError = require('http-errors');
const pool = require('../db');
const { subtaskSchema } = require('../helpers/validation');

module.exports = {
  index: async (req, res, next) => {
    try {
      const result = await pool.query('SELECT\
    * FROM subtasks');

      const { rows: allSubTasks } = result;

      res.json({ allSubTasks });
    } catch (error) {
      next(error);
    }
  },
  show: async (req, res, next) => {
    try {
      const { id: subtaskId = null } = req.params;

      if (isNaN(subtaskId)) {
        throw createError.BadRequest();
      }
      const result = await pool.query(
        'SELECT\
      * FROM subtasks WHERE id = $1',
        [subtaskId]
      );

      if (result.rows.length === 0) {
        throw createError.NotFound();
      } else {
        const subTask = result.rows[0];

        res.json({ subTask });
      }
    } catch (error) {
      next(error);
    }
  },
  create: async (req, res, next) => {
    try {
      const response = await subtaskSchema.validateAsync(req.body);

      const { subtask, status, task_id } = response;

      const userId = req.payload.aud;

      // SEE IF THE TASK_ID BELONGS TO THE USER
      const check = await pool.query(
        'SELECT\
      * FROM tasks WHERE id = $1 AND\
      user_id = $2',
        [task_id, userId]
      );

      // IF NOT THROW AN ERROR
      if (check.rows.length === 0) {
        throw createError.Unauthorized();
      }

      // ELSE INSERT THE SUBTASK
      const result = await pool.query(
        'INSERT \
      INTO subtasks (subtask, status, task_id)\
      VALUES ($1,$2,$3) RETURNING *',
        [subtask, status, task_id]
      );
      const newSubTask = result.rows[0];

      res.status(201).send({ newSubTask });
    } catch (error) {
      if (error.isJoi === true) {
        error.status = 400;
      }
      next(error);
    }
  },
  update: async (req, res, next) => {
    try {
      const { id: subtaskId = null } = req.params;

      if (isNaN(subtaskId)) {
        throw createError.BadRequest();
      }
      const response = await subtaskSchema.validateAsync(req.body);

      const { subtask, status, task_id } = response;

      const userId = req.payload.aud;

      // SEE IF THE TASK_ID BELONGS TO THE USER
      const check = await pool.query(
        'SELECT\
    * FROM tasks WHERE id = $1 AND\
    user_id = $2',
        [task_id, userId]
      );

      // IF NOT THROW AN ERROR
      if (check.rows.length === 0) {
        throw createError.Unauthorized();
      }

      // ELSE UPDATE THE SUBTASK
      const result = await pool.query(
        'UPDATE\
      subtasks SET subtask = $1, status = $2\
      WHERE id = $3 AND task_id = $4 RETURNING *',
        [subtask, status, subtaskId, task_id]
      );
      if (result.rows.length === 0) {
        throw createError.Unauthorized();
      }
      const updatedSubTask = result.rows[0];

      res.send({ updatedSubTask });
    } catch (error) {
      if (error.isJoi === true) error.status = 400;

      next(error);
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id: subtaskId = null } = req.params;
      const { task_id = null } = req.body;

      if (isNaN(subtaskId) || task_id === null) {
        throw createError.BadRequest();
      }
      const userId = req.payload.aud;

      // SEE IF THE TASK_ID BELONGS TO THE USER
      const check = await pool.query(
        'SELECT\
         * FROM tasks WHERE id = $1 AND\
           user_id = $2',
        [task_id, userId]
      );

      // IF NOT THROW AN ERROR
      if (check.rows.length === 0) {
        throw createError.Unauthorized();
      }

      const result = await pool.query(
        'DELETE\
      FROM subtasks WHERE id = $1 AND task_id = $2 RETURNING *',
        [subtaskId, task_id]
      );

      if (result.rowCount === 0) {
        throw createError.Unauthorized();
      }

      const deletedSubTask = result.rows[0];
      res.send({ deletedSubTask });
    } catch (error) {
      next(error);
    }
  },
};
