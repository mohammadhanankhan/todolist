const createError = require('http-errors');
const bcrypt = require('bcrypt');
const pool = require('../db');
const { authSchema } = require('../helpers/validation');
const {
  signAccessToken,
  signRefreshToken,
  verifyRefreshToken,
} = require('../helpers/jwt');
const client = require('../helpers/init_redis');

module.exports = {
  register: async (req, res, next) => {
    try {
      // VALIDATE INPUT / SANITIZE INPUT
      const result = await authSchema.validateAsync(req.body);

      // CHECK IN DATABASE IF USER ALREADY EXISTS
      const user = await pool.query(
        'SELECT email, password FROM users WHERE email = $1',
        [result.email]
      );

      const storedUser = user.rows[0];

      if (storedUser !== undefined) {
        throw createError.Conflict(`${result.email} already exists`);
      }
      // HASH PASSWORD
      const hashedPassword = await bcrypt.hash(result.password, 12);

      // INSERT A USER
      const newUser = await pool.query(
        'INSERT INTO users (email, password) \
       VALUES ($1,$2) RETURNING *',
        [result.email, hashedPassword]
      );

      const newUserCredentials = newUser.rows[0];

      const userId = newUserCredentials.id;

      const accessToken = await signAccessToken(userId);
      const refreshToken = await signRefreshToken(userId);

      res.send({ accessToken, refreshToken });
    } catch (error) {
      // CHECK IF ERROR IS COMING FROM JOI IF TRUE CHANGE THE STATUS FROM 500 TO SAY (422 - UNPROCESSABLE ENTITY)
      if (error.isJoi === true) error.status = 422;
      next(error);
    }
  },
  login: async (req, res, next) => {
    try {
      const result = await authSchema.validateAsync(req.body);

      const user = await pool.query(
        'SELECT id, email, password FROM users WHERE email = $1',
        [result.email]
      );

      const credentials = user.rows[0];

      if (credentials === undefined) {
        throw createError.NotFound("User doesn't exist");
      }

      const storedPassword = credentials.password;

      // CHECK IF THE PASSWORD THAT USER ENTERED MATCHES
      const isMatch = await bcrypt.compare(result.password, storedPassword);

      if (!isMatch) {
        throw createError.Unauthorized('Invalid Email/Password ');
      }

      const userId = credentials.id;

      const accessToken = await signAccessToken(userId);
      const refreshToken = await signRefreshToken(userId);

      res.send({ accessToken, refreshToken });
    } catch (error) {
      if (error.isJoi === true) {
        return next(createError.BadRequest('Invalid Email/Password'));
      }
      next(error);
    }
  },
  refreshToken: async (req, res, next) => {
    try {
      const { refreshToken = null } = req.body;

      if (refreshToken === null) {
        throw createError.BadRequest();
      }

      const userId = await verifyRefreshToken(refreshToken);

      const accessToken = await signAccessToken(userId);
      const refToken = await signRefreshToken(userId);

      res.send({ accessToken: accessToken, refreshToken: refToken });
    } catch (error) {
      next(error);
    }

    res.send('refresh-token route');
  },
  logout: async (req, res, next) => {
    try {
      const { refreshToken = null } = req.body;
      if (refreshToken === null) {
        throw createError.BadRequest();
      }
      const userId = await verifyRefreshToken(refreshToken);
      client.DEL(userId, (err, val) => {
        if (err) {
          console.log(err.message);

          throw createError.InternalServerError();
        } else {
          // IN THIS CASE IT MEANS EITHER THE KEY HAS BEEN DELETED OR THE KEY DOESN'T EXIST - IN ANY OF THE USER HAS BEEN LOGGED OUT
          console.log(val);

          res.sendStatus(204); // THERE'S NO RESPONSE BUT EVERYTHING WENT OK
        }
      });
    } catch (error) {
      next(error);
    }
  },
};
