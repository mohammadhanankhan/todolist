const createError = require('http-errors');
const pool = require('../db');
const { taskSchema } = require('../helpers/validation');

module.exports = {
  index: async (req, res, next) => {
    try {
      const result = await pool.query('SELECT * FROM tasks');

      const { rows: allTasks } = result;

      res.json({ allTasks });
    } catch (error) {
      next(error);
    }
  },
  show: async (req, res, next) => {
    try {
      const { id: taskId = null } = req.params;

      if (isNaN(taskId)) {
        throw createError.BadRequest();
      }

      const result = await pool.query('SELECT * FROM tasks WHERE id = $1', [
        taskId,
      ]);

      if (result.rows.length === 0) {
        throw createError.NotFound();
      } else {
        const task = result.rows[0];

        res.json({ task });
      }
    } catch (error) {
      next(error);
    }
  },
  create: async (req, res, next) => {
    try {
      const response = await taskSchema.validateAsync(req.body);

      const userId = req.payload.aud;

      const result = await pool.query(
        'INSERT INTO tasks(task, status, user_id)\
        VALUES  ($1,$2,$3) RETURNING *',
        [response.task, response.status, userId]
      );

      const newTask = result.rows[0];

      res.status(201).send({ newTask });
    } catch (error) {
      if (error.isJoi === true) {
        error.status = 400;
      }
      next(error);
    }
  },
  update: async (req, res, next) => {
    try {
      const { id: taskId = null } = req.params;

      if (isNaN(taskId)) {
        throw createError.BadRequest();
      }

      const response = await taskSchema.validateAsync(req.body);

      const userId = req.payload.aud;

      const result = await pool.query(
        'UPDATE tasks SET task = $1, status = $2 WHERE id = $3 AND user_id = $4 RETURNING *',
        [response.task, response.status, taskId, userId]
      );
      if (result.rows.length === 0) {
        throw createError.Unauthorized();
      }
      const updatedTask = result.rows[0];

      res.send({ updatedTask });
    } catch (error) {
      if (error.isJoi === true) error.status = 400;

      next(error);
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id: taskId = null } = req.params;

      if (isNaN(taskId)) {
        throw createError.BadRequest();
      }

      const userId = req.payload.aud;

      const result = await pool.query(
        'DELETE FROM tasks \
      WHERE id = $1 AND user_id = $2 RETURNING *',
        [taskId, userId]
      );

      if (result.rowCount === 0) {
        throw createError.Unauthorized();
      }

      const deletedTask = result.rows[0];
      res.send({ deletedTask });
    } catch (error) {
      next(error);
    }
  },
};
