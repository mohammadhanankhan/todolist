const express = require('express');
const router = express.Router();
const { verifyAccessToken } = require('../helpers/jwt');
const SubtasksController = require('../controllers/subtasks.controller');

router.use(verifyAccessToken);

// GET ALL SUBTASKS
router.get('/', SubtasksController.index);

// GET SINGLE SUBTASK
router.get('/:id', SubtasksController.show);

// CREATE NEW SUBTASK
router.post('/', SubtasksController.create);

// UPDATE SUBTASK
router.patch('/:id', SubtasksController.update);

// DELETE SUBTASK
router.delete('/:id', SubtasksController.delete);

module.exports = router;
