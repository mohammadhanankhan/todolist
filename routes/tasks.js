const express = require('express');
const router = express.Router();
const { verifyAccessToken } = require('../helpers/jwt');
const TasksController = require('../controllers/tasks.controller');

router.use(verifyAccessToken);

// GET ALL TASKS
router.get('/', TasksController.index);

// GET SINGLE TASK
router.get('/:id', TasksController.show);

// CREATE NEW TASK
router.post('/', TasksController.create);

// UPDATE TASK
router.patch('/:id', TasksController.update);

// DELETE TASK
router.delete('/:id', TasksController.delete);

module.exports = router;
